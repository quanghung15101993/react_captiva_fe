import { GET_ALL_PROJECT } from "../types";

export const getAll = () => async (dispatch) => {
  try {
    /*const response = await axios.get(
        "https://jsonplaceholder.typicode.com/todos?_limit=3"
      );*/
    console.log("Action");
    const response = [
      {
        key: "1",
        name: "John Brown",
        age: 32,
        address: "New York No. 1 Lake Park",
        tags: ["nice", "developer"],
      },
      {
        key: "2",
        name: "Jim Green",
        age: 42,
        address: "London No. 1 Lake Park",
        tags: ["loser"],
      },
      {
        key: "3",
        name: "Joe Black",
        age: 32,
        address: "Sidney No. 1 Lake Park",
        tags: ["cool", "teacher"],
        user_type: 1,
      },
    ];
    dispatch({
      type: GET_ALL_PROJECT,
      payload: response,
    });
  } catch (error) {
    console.log(error);
  }
};
