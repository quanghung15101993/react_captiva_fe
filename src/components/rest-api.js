const localhost = "http://127.0.0.1:8000";

const getAllRecogProfile = localhost + "/rest/cap/reg-profiles";
const getAllProject = localhost + "/rest/cap/project";

export default { getAllRecogProfile, getAllProject };
