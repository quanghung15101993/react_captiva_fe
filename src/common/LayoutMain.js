import React, { useState } from "react";
import { Layout, Menu } from "antd";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {
  AppstoreOutlined,
  PieChartOutlined,
  BarsOutlined,
  ContainerOutlined,
} from "@ant-design/icons";
import ContentCommon from "./Content";
import FooterCommon from "./Footer";
import HeaderCommon from "./Header";
import Sider from "antd/es/layout/Sider";
import BreadcrumbCommon from "./Breadcrumb";
import routes from "./ConstRoute";

const { Content } = Layout;

function LayoutMain() {
  const [collapsed, setCollapsed] = useState(false);
  return (
    <Router>
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={() => setCollapsed(!collapsed)}
      >
        <div className="logo" />
        <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
          <Menu.Item key="1" icon={<AppstoreOutlined />}>
            <Link to="/project">Project</Link>
          </Menu.Item>
          <Menu.Item key="2" icon={<PieChartOutlined />}>
            <Link to="/ip-project">IP Project</Link>
          </Menu.Item>
          <Menu.Item key="3" icon={<PieChartOutlined />}>
            <Link to="/ip-flow">IP Flow</Link>
          </Menu.Item>
          <Menu.Item key="4" icon={<BarsOutlined />}>
            <Link to="/recog-profile">Recognition profile</Link>
          </Menu.Item>
          <Menu.Item key="5" icon={<ContainerOutlined />}>
            <Link to="/templates">Templates</Link>
          </Menu.Item>
          <Menu.Item key="6" icon={<PieChartOutlined />}>
            <Link to="/extraction">Extraction</Link>
          </Menu.Item>
          <Menu.Item key="8" icon={<PieChartOutlined />}>
            <Link to="/testcase">Test Case</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <HeaderCommon
          className="site-layout-background"
          style={{ padding: 0 }}
        />

        <Switch>
          {routes.map((route, i) => (
            <RouteWithSubRoutes key={i} {...route} />
          ))}
        </Switch>
        <FooterCommon />
      </Layout>
    </Router>
  );
}

/*A special wrapper for <Route> that knows how to
 handle "sub"-routes by passing them in a `routes`
prop to the component it renders.*/
function RouteWithSubRoutes(route) {
  return (
    <Route path={route.path}>
      <Content style={{ margin: "0 16px" }}>
        <BreadcrumbCommon page={route.breadcrumb} />
        <ContentCommon id={route.breadcrumb} />
      </Content>
    </Route>
  );
}

export default LayoutMain;
