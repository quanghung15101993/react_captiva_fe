import React from "react";
import Project from "../components/project/Project";
import RecogProfile from "../components/recog-profile/RecogProfile";

function ContentCommon(props) {
  let compo;
  switch (props.id) {
    case "project":
      compo = <Project />;
      break;
    case "recog-profile":
      compo = <RecogProfile />;
      break;
    default:
      break;
  }
  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 360 }}
    >
      {compo}
    </div>
  );
}

export default ContentCommon;
