const breadcrumbs = [
  {
    page: "project",
    name: "Project",
  },
  {
    page: "ip-project",
    name: "Image Processing Project",
  },
  {
    page: "ip-flow",
    name: "Image Processing Flow",
  },
  {
    page: "recog-profile",
    name: "Recognition Profile",
  },
  {
    page: "templates",
    name: "Template",
  },
  {
    page: "extraction",
    name: "Extraction",
  },
  {
    page: "testcase",
    name: "Test Case",
  },
];

export default breadcrumbs;
