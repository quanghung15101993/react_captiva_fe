import LayoutMain from './LayoutMain';
import ContentCommon from './Content';
import FooterCommon from './Footer';
import HeaderCommon from './Header';
export { LayoutMain, ContentCommon, FooterCommon, HeaderCommon }